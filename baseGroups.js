module.exports = [
    {
      name: 'You know you\'re from Carlisle PA when...',
      slug: '61834504324',
    },
    {
      name: 'Carlisle Barracks Community Info Page',
      slug: '1323609964394020',
    },
    {
      name: 'I\'m so Carlisle...',
      slug: '1515435445356278',
    },
    {
      name: 'The community of Carlisle!',
      slug: 'CarlisleAsACommunity',
    },
    {
      name: 'Whats going on in Mt. Holly Springs, PA',
      slug: 'mthollysprings',
    },
    {
      name: 'Everything Newville',
      slug: '627116124085812',
    },
    {
      name: 'Newville Info',
      slug: '468072440039860',
    },
    {
      name: 'Shippensburg Community',
      slug: '471097279736309',
    },
    {
      name: 'Shippensburg Info and Opinions',
      slug: '703037529834104',
    },
    {
      name: 'What\'s going on in Chambersburg',
      slug: 'whatsgoingoninchambersburg',
    },
    {
      name: 'What\'s Happening in Camp Hill',
      slug: 'WhatsHappeningInCampHillPA',
    },
    {
      name: 'Boiling Springs (Pennsylvania) Happenings',
      slug: '172360349616176',
    },
];

