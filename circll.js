
class Node {
  val = null;
  next = null;
  constructor(data, next) {
    this.val = data;
    this.next = next;
  }
}

class CircLL {
  #head = null;
  #cur = null;
  constructor(...args) {
    console.log("construct args:",args);
    this.#head = new Node(args.shift(), null);
    console.log("construct args after shift:",args);
    this.#head.next = this.#head;
    this.#cur = this.#head;
    this.add.apply(this,args);
  }

  add(...args) {
    console.log("args: ",args);
    args.forEach((val) => {
      const nextVal = new Node(val, this.#head);
      while(this.#cur.next !== this.#head) {
        console.log("cur next !== head");
        this.next();
      }
      this.#cur.next = nextVal;
      this.next();
    })
  }

  get() {
    return this.#cur.val;
  }

  rewind() {
    this.#cur = this.#head;
  }

  set(newVal) {
    this.#cur.val = newVal;
  }

  next() {
    const next = this.#cur.next;
    this.#cur = next;
    return next;
  }
}

module.exports = {
  Node,
  CircLL,
};
