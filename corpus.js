module.exports = {
  fullNames: {
    petroleucarlisle: 'Carlisle Petroleum',
    gracefullheart: 'Gracefull Heart',
    beitlerschimneys: 'Beitler\'s Chimneys',
    centralpascreenp: 'Central PA Screen Printing',
    farnhaminsurance: 'Farnham Insurance',
    macslashandskins: 'MAC\'s Lash and Skin Studio',
  },
  corpus: {
    businesses: {
      petroleucarlisle: [
        'heating oil',
        'furnace',
        'propane',
        'hvac repair',
        'fireplace',
        'plumbing',
        'air conditioning',
        'air conditioner',
        'hvac maintenance',
        'heat installation',
        'ac installation',
        'humidifiers',
      ],
      gracefullheart: [
        'reiki training',
        'tumbled stones',
        'salt lamps',
        'aura photography',
        'workshops',
        'reiki healing',
        'stargate',
        'crystals',
        'metaphysical',
        'orgonite',
        'essential oils',
        'gemstone jewelry',
        'incense',
        'candles',
        'crystal bowls',
        'spiritual services',
        'sacred spirals healing',
        'cbd oil',
      ],
      beitlerschimneys: [
        'chimney sweep',
        'fireplace repair',
        'firelace service',
        'fireplace maintenance',
        'masonry repair',
        'stainless steel liner installation',
        'chimney caps',
        'chimney inspection',
        'chimney flashing',
        'chimney waterproofing',
        'chimney crown repair',
      ],
      centralpascreenp: [
        'screen printing',
        'vinyl transfers',
        'sublimation printing',
        'custom t-shirt design',
        'tshirt design',
        'tee shirt design',
        'infused ink printing',
        'dye sublimation',
      ],
      farnhaminsurance: [
        'automobile insurance',
        'car insurance',
        'motorcycle insurance',
        'life insurance',
        'property insurance',
        'home insurance',
        'renters insurance',
        'mobile home insurance',
        'condo insurance',
        'flood insurance',
        'commercial insurance',
        'business insurance',
        'rv insurance',
        'boat insurance',
        'snowmobile insurance',
        'specialty vehicle insurance',
      ],
      macslashandskins: [
        'makeup lessons',
        'eyelash extensions',
        'extensions',
        'bridal makeup',
        'airbrush makeup',
        'facial',
        'treatment facial',
        'microcurrent facial',
        'microchannel',
        'microdermabrasion',
        'dermaplaning',
        'spray tan',
        'light therapy',
        'waxing',
        'skin care',
        'hair care',
        'lashes',
        'eyelashes',
        'cosmetics',
        'eyebrows',
        'lash lift',
        'lash tint',
        'microblading',
      ],
      guardiandesigns: [
        'web presence management',
        'web design',
        'graphic design',
        'web development',
        'call tracking',
        'managed hosting',
        'website hosting',
        'internet marketing',
        'dedicated hostig',
        'sms marketing',
        'text marketing',
        'dropshipping management',
        'seo',
        'search engine optimization',
        'business listing management',
        'email management',
        'email marketing',
        'social media coaching',
        'analytics',
        'drop-shipping',
        'wholesale management',
      ],
    },
  },
};
