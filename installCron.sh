#!/usr/bin/env bash

function addCron() {
  [ -z $1 ] && echo 'i need a username' && return 255;
  local user=`basename $1 .js`;
  local nalp="/home/$user/public_html/nalpa.php"
  local cron="/var/spool/cron/$user"
  local domain=`/scripts/userdomains $user | grep -v guardian | head -n 1 | tr -d '\n'`
  local entry="*/5 * * * * wget -O /dev/null https://$domain/nalpa.php --user-agent=\"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36\" > /dev/null 2>&1"
  [ ! -f $cron ] & touch $cron;
  if ! grep nalpa.php $cron; then
    if [ ! -f $nalp ]; then
      echo "adding nalpa.php to $user's web dir"
      cp nalpa.php $nalp;
    fi
    echo "installing nalpa cron entry for $user";
    echo "$entry" >> $cron;
    echo "now check the crontab for $user ($domain)";
    read;
    su $user -c 'crontab -e'
  else
    echo "not doing stuff for $user uwu"
  fi
}

for i in pages/*.js; do
  addCron $i
done
