#!/usr/bin/env node
'use strict';
const puppeteer = require('puppeteer');
const auth = require('./credentials.js'); // login credentials*/
const PushBullet = require('pushbullet'); // pushbullet for sharing photos
const pb = new PushBullet(auth.pbKey);
const {NlpManager} = require('node-nlp');
const fs = require('fs');
const {ObjectId, MongoClient} = require('mongodb');
const MongoUrl = 'mongodb://localhost:27017';
const Redis = require('redis'); // pub/sub rubadubdub
const {ueQue} = require('./ueQue.js');
const {stopwords} = require('./stopwords.js');
const AwaitLock = require('await-lock').default;
const sw = require('stopword');
const mail = require('nodemailer');
const natural = require('natural');
const resolve = require('resolve');

const transport = mail.createTransport(auth.smtp);

const recLock = new AwaitLock();
const slaveLock = new AwaitLock();
const browserLock = new AwaitLock();


/** *
 * NAturaLinguaProcessA
 *
 */
class Nalpa {
  #browser = undefined;
  #workers = [];
  #baseGroups = [];
  mongo = undefined;
  db = undefined;
  /**
  * constructor for nalpa master
  * @param {Object} opts - options!!!11!11!elf!
  */
  constructor(opts) {
    this.#baseGroups = require(__dirname+'/baseGroups.js');
    this.headless = typeof opts.headless === 'undefined' ? true : opts.headless;
    this.pathPrefix = typeof opts.pathPrefix !== 'undefined' ?
                      opts.pathPrefix : '/var/node/nalpa';
    // const baseDir = this.pathPrefix;
    // this.group = targetGroup;
    return (async () => {
      console.log('launching browser');
      this.#browser = await puppeteer.launch({
        headless: this.headless,
        userDataDir: this.pathPrefix+'/data',
        args: [
          '--no-sandbox',
          '--process-per-site',
          '--renderer-process-limit=5',
        ],
        slowMo: 10,
      });
      const context = this.#browser.defaultBrowserContext();
      await context.overridePermissions('https://www.facebook.com', ['notifications']);
      let page = await this.#browser.newPage();
      page.setViewport({width: 1900, height: 1600, deviceScaleFactor: 1});
      this.mongo = await MongoClient.connect(MongoUrl, {
        useUnifiedTopology: true,
      });
      this.db = this.mongo.db('nalpa');
      this.posts = this.db.collection('posts');
      this.notify = this.db.collection('notify');

      // navigate to fb
      await page.goto('https://facebook.com');

      try {
        await page.waitForSelector(
            this.sel.login.submit,
            {timeout: 5000},
        );
        await fblogin(page);
      } catch (e) { // already logged in
        console.log('already logged in to fb');
      } finally {
        // get posts for target group
        console.log('nalpa initialized!');
        await page.goto('about:blank');
        await page.close();
        page = null;
        return this;
      }
    })();
  }

  /**
  * dump workers
  */
  dumpWorkers() {
    console.table(this.#workers);
  }

  /**
  * get pending post from db
  * @param {string} id - mongodb id
  */
  async getPendingPost(id) {
    const mid = new ObjectId(id);
    const res = this.notify.findOne({'_id': mid});
    if (!res) {
      return false;
    }
    return res;
  }
  /**
  * perform pending post ! ! !
  * @param {string} id - mongodb id
  */
  async doPost(id) {
    console.log('Received request to post pending comment (db id:', id+')');
    const post = this.getPendingPost(id);
    if (!post) {
      console.log('Could not find post.');
    } else {
      const worker = await this.getWorker(res.page);
      worker.doPost(post);
    }
  }

  /**
  * delete comment owo
  * @param {string} id - id of comment entry in mongo
  */
  async deleteComment(id) {
    const mid = new ObjectId(id);
    console.log('Received request to delete comment (db id:', id+')');
    const post = await this.posts.findOne({'_id': mid});
    if (!post) {
      console.log('Unable to find that post in the database.');
      return false;
    } else {
      const worker = await this.getWorker(post.page);
      await worker.deleteComment(post);
      return post;
    }
  }

  /**
  * dump individual worker
  * @param {string} origin - worker to dump
  */
  async dump(origin) {
    const worker = await this.getWorker(origin);
    // console.table(worker);
    worker.dump();
  }

  /**
  * close browser
  */
  async closeBrowser() {
    this.#browser.close();
    this.mongo.close();
    this.#browser = null;
  }
  /**
   * Coordinates for screenshot
   * @typedef {Object} Coordinates
   * @property {int} x - left edge of element
   * @property (int) y - top edge of element
   * @property {number} width - width of element
   * @property {number} height - height of element
   */

  /**
   * Options for screenshot
   * @typedef {Object} ScreenshotOpts
   * @property {string=} filePrefix - prefix for filename
   * @property {string=} filePath - path to save file
   * @property {Coordinates=} coords - optional {@link Coordinates} to target
   */


  /**
   * capture screenshot of a given page
   * @param {Page} page - an active puppeteer page
   * @param {ScreenshotOpts=} opts - optional arguments
   */
  async screenshot(page, opts) {
    const defaults = {
      filePrefix: 'fb',
      filePath: '/tmp',
    };
    opts = Object.assign(defaults, opts);
    // console.dir(opts);
    // const target = null;
    const nonce = typeof opts.nonce === 'undefined' ?
      '-'+Date.now() : opts.nonce;
    const filename = `${opts.filePrefix}${nonce}.png`; // filename
    const path = `${opts.filePath}/${filename}`;
    const title = typeof opts.title === 'undefined' ?
                  filename :
                  opts.title;
    const screenshotOpts = {
      path,
    };
    // if (typeof opts.element === 'undefined') {
    //  console.log('did not receive element object');

    if (typeof opts.coords === 'undefined' || this.isEmptyObject(opts.coords)) {
      console.log('capturing full page');
      screenshotOpts.fullPage = true;
    } else {
      console.log('capturing clipped area');
      screenshotOpts.clip = opts.coords;
    }

    //  target = page;
    // } else {
    //  console.log('capturing specific element');
    //  target = opts.element;
    // }

    console.log('capturing screenshot to '+path);
    await page.screenshot(screenshotOpts);

    console.log('pushing screenshot to pushbullet');
    pb.file({}, path, title, (e, r) => {
      if (e) {
        console.error('Error pushing photo: ', e);
      } else {
        console.log(title, 'pushed to pushbullet successfully');
      }
    });
  }


  /**
   * check if object is empty
   * @param {Object} obj - object to check
   * @return {boolean} true if object is empty
   */
  isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  }

  /**
   * login to facebook
   * @param {Page} page - active puppeteer page
   */
  async fblogin(page) {
    // focus username field and type it
    // console.log('logging in to the book of feces');
    console.log('inputting username');
    await page.focus(this.sel.login.user);
    await page.keyboard.type(auth.username);

    // password field
    console.log('inputting password');
    await page.focus(this.sel.login.pass);
    await page.keyboard.type(auth.password);

    // click login
    console.log('submitting login form');
    await page.click(this.sel.login.submit);
    await page.waitForNavigation();
    console.log('Logged In!');
  }
  /**
  * update base groups
  */
  getBase() {
    const resPath = resolve.sync(__dirname+'/baseGroups.js');
    if (require.cache[resPath]) {
      delete require.cache[resPath];
    }
    const newBase = require(__dirname+'/baseGroups.js');
    if (newBase.length != this.#baseGroups.length) {
      console.log('updating baseGroups');
      this.#baseGroups = require(__dirname+'/baseGroups.js');
    }
  }

  /**
   * trigger a check for a given page
   * @param {string} origin - name of page
   */
  async trigger(origin) {
    //console.log('triggering', origin);
    this.getBase();
    const worker = await this.getWorker(origin);
    console.log('running check for', origin);
    await worker.run(this.#baseGroups);
  }

  /**
  * get worker instance
  * @param {string} origin - page name
  */
  async getWorker(origin) {
    //console.log('getting worker '+origin);
    let worker;
    if (typeof (
      worker = this.#workers.find((e) => e.usr === origin)
    ) === 'undefined') {
      console.log('worker not created for', origin + ', creating one now.');
      worker = await new NalpaWorker(
          origin,
          this.#browser,
          this.#baseGroups,
          this,
      );
      this.#workers.push(worker);
    }
    return worker;
  }
  /**
  * retrain model
  * @param {string} origin - which page to retrain for
  */
  async retrain(origin) {
    console.log('retraining '+origin);
    const worker = await this.getWorker(origin);
    await worker.trainModel();
  }
  /**
  * reload config
  * @param {string} origin - page to reload config for
  */
  async reload(origin) {
    console.log('reloading '+origin);
    const worker = await this.getWorker(origin);
    this.getBase();
    worker.loadConfig(this.#baseGroups);
  }
}

/**
 * nalpa worker class, handles the heavy lifting!
 *
 */
class NalpaWorker {
  #groups = null;
  page = null;
  #config = null;
  #browser = null;
  usr = null;
  mongo = null;
  db = null;
  col = null;
  notify = null;
  parent = null;
  #manager = new NlpManager({
    languages: ['en'],
    nlu: {
      // useNoneFeature: false,
    },
  });
  /**
  * create new worker
  * @param {Page} page - puppeteer page
  * @param {Object} puppeteer - base puppeteer object
  * @param {Object[]} baseGroups - list of base groups
  * @param {Nalpa} parent - slave driver
  */
  constructor(page, puppeteer, baseGroups, parent) {
    return (async () => {
      this.parent = parent;
      this.sel = { // css selectors
        posts: { // selectors for page elements
          container: 'div.userContentWrapper', // selector for a post
          message: '.userContent',
          input: 'form.commentable_item [contenteditable=true]',
          permalink: 'a._5pcq',
        },
      };
      // console.log('constructing worker for', page);
      this.#browser = puppeteer;
      this.page = await this.#browser.newPage();
      this.usr = page;
      this.mongo = await MongoClient.connect(MongoUrl, {
        useUnifiedTopology: true,
      });
      this.db = this.mongo.db('nalpa');
      this.col = this.db.collection('posts');
      this.notify = this.db.collection('notify');
      const path = `${__dirname}/pages/${page}.js`;
      try {
        fs.accessSync(path, fs.constants.F_OK);
      } catch (e) {
        throw new Error(`Unable to find page: ${page}`);
      }
      this.loadConfig(baseGroups);
      const modelPath = `${__dirname}/models/${page}.nlp`;
      /* try {
        fs.accessSync(modelPath, fs.constants.F_OK);
        // console.log('loading', modelPath);
        this.#manager.load(modelPath);
      } catch (e) {
        // console.log('model not found. training for', this.usr+'...');
      } */
      await this.trainModel();
      return this;
    })();
  }
  /**
  * browser de klose
  */
  async closeBrowser() {
    this.#browser.close();
    // this.mongo.close();
    this.#browser = undefined;
  }
  /**
  * train nlp model
  */
  async trainModel() {
    console.log(`training model for ${this.usr}`);
    const modelPath = `${__dirname}/models/${this.usr}.nlp`;
    try {
      fs.accessSync(modelPath, fs.F_OK);
      fs.unlinkSync(modelPath);
    } catch {}
    this.reconfig();
    this.#manager = new NlpManager({
      languages: ['en'],
      nlu: {
        // useNoneFeature: false,
      },
    });
    const corpus = this.#config.corpus;
    for (const klass in corpus) {
      for (const phrase of corpus[klass]) {
        if (!phrase) {
          continue;
        }
        console.log(this.usr, '- adding phrase \''+phrase+
         '\' to NLP manager under class:'+klass,
        );
        this.#manager.addDocument('en', phrase, klass);
      }
    }
    await this.#manager.train();
    this.#manager.save(modelPath);
  }
  /**
  * clear require cache for config file uwu
  */
  reconfig() {
    const path = `${__dirname}/pages/${this.usr}.js`;
    const resPath = resolve.sync(path);
    if (require.cache[resPath]) {
      delete require.cache[resPath];
    }
    if (this.#config) {
      this.#config = null;
    }
    this.#config = require(path);
  }
  /**
  * configger
  * @param {Group[]} baseGroups - list of base groups
  */
  loadConfig(baseGroups) {
    this.reconfig();
    const groupExclude = this.#config.data.groups.exclude;
    const includes = this.#config.data.groups.include;
    const sub = baseGroups.filter((e) => !groupExclude.includes(e.slug));
    if (this.#groups) {
      this.#groups = null;
    }
    this.#groups = ueQue.fromArray(
        sub.concat(includes.filter((e) => !sub.some(
            (f) => f.slug.toLowerCase() === e.slug.toLowerCase(),
        ))),
    );
  }
  /**
   * run them jewels fast o/
   * @param {Object[]} baseGroups - possibly new base groups
   */
  async run(baseGroups) {
    console.log('got request to run for', this.usr);
    await slaveLock.acquireAsync();
    try {
      const cur = this.#groups.get();
      this.loadConfig(baseGroups);
      if (!this.#groups.length()) {
        return;
      }
      while (this.#groups.get().slug !== cur.slug) {
        this.#groups.deq();
      }
      await this.getPosts();
    } catch (e) {
      console.error('error in run:', e, 'for page:', this.usr);
    } finally {
      [slaveLock, browserLock, recLock].filter(
          (e) => e._acquired,
      ).forEach((e) => e.release());
      // slaveLock.release();
    }
  }
  /**
  * dump ur guts
  */
  dump() {
    console.log('config for '+this.usr);
    console.dir(this.#config.data);
    console.log('corpus for '+this.usr);
    console.table(this.#config.corpus);
    console.log('groups for '+this.usr);
    console.table(this.#groups.asArray());
  }
  /**
   * configure a given page object to try and
   * subvert headless detection
   * @param {Page} page - active puppeteer page
   */
  async evadeHeadlessDetection() {
    if (typeof this.page === 'undefined') {
      return;
    }
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await this.page.setUserAgent(userAgent);
    await this.page.evaluateOnNewDocument(() => {
      Object.defineProperty(navigator, 'webdriver', {
        get: () => false,
      });
      window.navigator.chrome = {
        app: {
          isInstalled: false,
        },
        webstore: {
          onInstallStageChanged: {},
          onDownloadProgress: {},
        },
        runtime: {
          PlatformOs: {
            MAC: 'mac',
            WIN: 'win',
            ANDROID: 'android',
            CROS: 'cros',
            LINUX: 'linux',
            OPENBSD: 'openbsd',
          },
          PlatformArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          PlatformNaclArch: {
            ARM: 'arm',
            X86_32: 'x86-32',
            X86_64: 'x86-64',
          },
          RequestUpdateCheckStatus: {
            THROTTLED: 'throttled',
            NO_UPDATE: 'no_update',
            UPDATE_AVAILABLE: 'update_available',
          },
          OnInstalledReason: {
            INSTALL: 'install',
            UPDATE: 'update',
            CHROME_UPDATE: 'chrome_update',
            SHARED_MODULE_UPDATE: 'shared_module_update',
          },
          OnRestartRequiredReason: {
            APP_UPDATE: 'app_update',
            OS_UPDATE: 'os_update',
            PERIODIC: 'periodic',
          },
        },
      };
      const originalQuery = window.navigator.permissions.query;
      return window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({state: Notification.permission}) :
        originalQuery(parameters)
      );
      Object.defineProperty(navigator, 'plugins', {
        get: () => 'oof'.split(''),
      });
      Object.defineProperty(navigator, 'languages', {
        get: () => ['en-US', 'en'],
      });
    });
  }
  /**
   * make the post!
   * @param {Object} post - fb post object thingy
   * @param {boolean=} force - whether to force post or not
   */
  async recPost(post, force) {
    await recLock.acquireAsync();
    try {
      const check = await this.checkPost(post);
      if (!check) {
        console.log(`navigating to ${post.permalink}`);
        await this.page.goto(post.permalink, {waitUntil: 'domcontentloaded'});
        console.log('post not in db, evaluating query... ');
        const stopped = sw.removeStopwords(
            post.query.toLowerCase().
              replace(/[^a-z0-9\s]/, ' ').
              split(/ /),
            sw.en.concat(stopwords)
        ).join(' ');
        const res = await this.#manager.process('en', stopped);
        console.log(this.usr, 'received query:', post.query);
        console.log('stopwords removed:', stopped);
        const intent = res.intent;
        console.log(this.usr, 'detected intent:', intent);
        console.log(res.classifications);
        if (intent == 'None' || intent == 'negative') {
          console.log(this.usr, 'did not match. skipping.');
          const insertID = await this.addPost(post);
          return false;
        }
        // console.log(res);
        const positiveConfidence = res.classifications.find(
            (e) => e.intent == 'positive',
        ).score;
        const negativeConfidence = res.classifications.find(
            (e) => e.intent == 'negative',
        );
        const confidence = (
          (positiveConfidence - (
            negativeConfidence ? negativeConfidence.score : 0
          )) * 100).toFixed(2);
        post.confidence = confidence;
        console.log(
            `${this.#config.data.fullName} matches`,
            `with ${confidence}% confidence`,
        );
        if (confidence <= 75 && !force) {
          const notifyed = await this.checkNotify(post);
          console.log("checkNotify: ", notifyed);
          if (confidence > 50) {
            if(!notifyed) {
            const subject = '[Guardian Designs] '+
            'Possible recommendation match for ' +
              this.#config.data.fullName;
            let text = 'Possible recommendation for ';
            text += `${this.#config.data.fullName} detected in`;
            text += ` ${post.group.name}\r\n`;
            text += `Query was: ${post.query}\r\n`;
            text += `Confidence of match: ${confidence}%\r\n`;
            text += `Post URL: ${post.permalink}\r\n`;
            const id = await this.addNotify(post);
            text += 'If this is a match and you '+
            'would like the recommmendation posted,';
            text += `please go to `+
                `https://guardian-designs.com/recommender/${id}/post\r\n`;
            text += `Thank you!`;
            console.log(subject);
            console.log('Sending notification email');
            this.notificate(subject, text);
            } else {
              console.log('notification already sent. ignoring.');
            }
          } else {
            console.log('Not confident enough. skipping.');
          }
          return false;
        }
        if(force) {
          console.log('got request to perform post of comment for '+this.usr+' on '+post.permalink);
        }
        try {
          await this.clickwait('.uiLayer ._5nxj', 10000);
        } catch (e) {
          console.log('nop no layarz');
        }
        await this.page.evaluate(() => {
          document.querySelectorAll('div[aria-label=comment i] > div > ._42ef').forEach(
              (e) => e.classList.remove('_42ef'),
          );
        });
        try {
          await this.page.waitForSelector(this.sel.posts.input);
        } catch (e) {
          console.log('unable to find input');
          return;
        }
        await this.clickwait(this.sel.posts.input, 5000);
        await this.page.keyboard.type(this.#config.data.fullName);
        await this.page.keyboard.press('Enter');
        await this.page.waitFor(1000);
        await this.page.waitForSelector('div[aria-label=comment i] > div > ._42ef a._6qw7', {timeout: 10000});
        const comments = await this.page.evaluate(() => {
          return Array.from(
              document.querySelectorAll(
                  'div[aria-label=comment i] > div > ._42ef',
              ),
          ).
              //filter((e) => e.querySelector('._42ef')).
              map((e) => {
                return {
                  text: e.querySelector('span._3l3x').innerText,
                  link: e.querySelector('a._6qw7').href,
                };
              });
        });
        const re = new RegExp(
            this.#config.data.fullName.
                replace(/[.*+?^${}()|[\]\\]/g, '\\$&'),
        );
        console.log('comments:',comments);
        const comment = comments.find((e) => e.text.match(re));
        post.commentID = comment.link.match(/comment_id=(\d+)/)[1];
        const insertID = await this.addPost(post);
        if (comment) {
          console.log('yup comment sending dat email');
          let text = `This is just a notification to let you know that we`;
          text += ` posted a recommendation for your `+
          `business (${this.#config.data.fullName})`;
          text += ` in ${post.group.name}`;
          text += '\n\n';
          text += `Recommendation request from post: ${post.query}\r\n`;
          text += `Confidence level: ${confidence}%\r\n`;
          text += `URL: ${post.permalink}\n\n`;
          text += `If this was made in error or you would`+
            ` like it removed, please go to `;
          text += `https://guardian-designs.com/recommender/${insertID}/delete `;
          text += `to have it automatically removed. Thank you!`;
          const subject = '[Guardian Designs] Recommendation posted for '+
            this.#config.data.fullName + ` in ${post.group.name}`;
          console.log(text);
          this.notificate(subject, text);
        }
      }
    } catch (e) {
      console.error('error in recPost:', e, 'for', this.usr);
    } finally {
      await this.page.goto('about:blank');
      // await this.page.close();
      // delete this.page;
      recLock.release();
    }
  }
  /**
  * send email
  * @param {string} subject - message subject
  * @param {string} text - message to send
  */
  notificate(subject, text) {
    const e = this.#config.emails;
    e.push('gary@guardian-designs.com', 'dan@guardian-designs.com');
    const emails = e.join(', ');
    const message = {
      from: 'Guardian Designs Facebook Recommendation'+
            ' <recommend@guardian-designs.com>',
      to: emails,
      subject,
      text,
    };
    transport.sendMail(message, (e, i) => {
      if (e) {
        console.error(e);
      } else {
        console.log('Notify email sent!');
      }
    });
  }
  /**
  * delete comment from post owo
  * @param {Object} res - mongo res
  */
  async deleteComment(res) {
    await browserLock.acquireAsync();

    if (!this.#browser) {
      this.#browser = await puppeteer.launch({
        headless: this.parent.headless,
        userDataDir: this.parent.pathPrefix+'/data',
        args: [
          '--no-sandbox',
        ],
        slowMo: 10,
      });
      const context = this.#browser.defaultBrowserContext();
      await context.overridePermissions('https://www.facebook.com', ['notifications']);
      this.page = await this.#browser.newPage();
      await this.evadeHeadlessDetection();
      this.page.setViewport({width: 1200, height: 1600, deviceScaleFactor: 1});
    }
    const slug = res.group;
    const postID = res.post_id;
    const commentID = res.comment_id;
    const group = this.#groups.asArray().find((e) => e.slug == slug);
    console.log(`Getting post ${postID} in ${group.name} [for ${this.usr}]`);
    const url = `https://facebook.com/groups/${group.slug}/permalink/${postID}?comment_id=${commentID}`;
    let ret;
    await this.page.goto(url, {waitUntil: 'domcontentloaded'});
    let skip = false;
    try {
      await this.page.waitForSelector(this.sel.posts.container, {
        timeout: 8000,
      });
    } catch (e) {
      console.log(this.usr, 'unable to load group. skipping!');
      skip = true;
    }
    try {
      if (!skip) {
        const commentFound = await this.page.evaluate((commentID) => {
          const delLink = Array.from(
              document.querySelectorAll(
                  'div[aria-label=comment i]',
              ),
          ).filter((e) => e.querySelector('._6qw7').href.match(
              new RegExp('comment_id='+commentID),
          ),
          )[0];
          if (delLink) {
            delLink.classList.add('bahleet-pls');
            return true;
          } else {
            return false;
          }
        }, commentID);
        if (commentFound) {
          await this.clickwait('.bahleet-pls button', 3000);
          await this.clickwait('.uiContextualLayer ._2so9 a', 3000);
          await this.clickwait('._4t2a a._4jy1', 3000);
          await this.page.waitFor(1500);
          try {
            await this.waitForSelector('.bahleet-pls');
            console.log('IT IS STILL HERE OWO');
          } catch (e) {
            console.log('Comment successfully deleted!');
            console.log('yup comment bahleeter  sedn dat email');
            let text = `This is a confirmation that the `+
              `recommendation you requested deleted, `;
            text += ` for https://facebook.com/groups/${group.slug}/permalink/${postID}`;
            text += ` has been deleted. Thank you!`;
            const subject = '[Guardian Designs] Recommendation deleted for '+
            this.#config.data.fullName + ` in ${group.name}`;
            this.notificate(subject, text);
          }
        }
      }
    } catch (e) {
      console.log(this.usr, 'error deleting comment: '+e );
    } finally {
      if (this.page) {
        await this.page.goto('about:blank');
        // await this.page.close();
        // this.page = undefined;
      }
      // await this.closeBrowser();
      if (browserLock._acquired) {
        browserLock.release();
      }
      ret = null;
    }
    return ret;
  }
  /**
  * perform pending post
  * @param {Object} res - mongo res
  */
  async doPost(res) {
    await browserLock.acquireAsync();

    if (!this.#browser) {
      this.#browser = await puppeteer.launch({
        headless: this.parent.headless,
        userDataDir: this.parent.pathPrefix+'/data',
        args: [
          '--no-sandbox',
        ],
        slowMo: 10,
      });
      const context = this.#browser.defaultBrowserContext();
      await context.overridePermissions('https://www.facebook.com', ['notifications']);
      this.page = await this.#browser.newPage();
      await this.evadeHeadlessDetection();
      this.page.setViewport({width: 1200, height: 1600, deviceScaleFactor: 1});
    }
    const slug = res.group;
    const postID = res.post_id;
    const group = this.#groups.asArray().find((e) => e.slug == slug);
    console.log(`Getting post ${postID} in ${group.name} [for ${this.usr}]`);
    const url = `https://facebook.com/groups/${group.slug}/permalink/${postID}`;
    let ret;
    console.log(url);
    await this.page.goto(url, {waitUntil: 'domcontentloaded'});
    console.log(`navigated to ${url}`);
    let skip = false;
    try {
      await this.page.waitForSelector(this.sel.posts.container, {
        timeout: 8000,
      });
    } catch (e) {
      console.log(this.usr, 'unable to load group. skipping!');
      skip = true;
    }
    try {
      if (!skip) {
        const posts = await this.page.evaluate((selectors, group) => {
          const gd = Array.from(
              document.querySelectorAll(selectors.container),
          );
          return gd.map((e) => {
            const child = e.querySelector(selectors.message);
            const permalink = e.querySelector(selectors.permalink).href;
            const id = permalink.match(/permalink\/(\d+)\//)[1];
            const {x, y, width, height} = e.getBoundingClientRect();
            return {
              query: child.innerText,
              id,
              permalink,
              idx: ++cnt,
              coords:
              {
                x, y, width, height,
              },
              group,
            };
          });
        }, this.sel.posts, group);
        if (posts.length > 0) {
          // console.log('requests for recommendations found');

          for (const post of posts) {
            await this.recPost(post, true);
          }
        } else {
          // console.log('no posts found');
        }
        ret = posts;
      }
    } catch (e) {
      console.log(this.usr, 'error doing pending post: '+e );
    } finally {
      if (this.page) {
        await this.page.goto('about:blank');
        // await this.page.close();
        // this.page = undefined;
      }
      // await this.closeBrowser();
      browserLock.release();
      ret = null;
    }
    return ret;
  }
  /**
   * get posts from post id
   * @param {Page} page - active puppeteer page
   * @param {boolean=} group - group slug
   */
  async getPosts() {
    const group = this.#groups.deq();
    await browserLock.acquireAsync();

    if (!this.#browser) {
      this.#browser = await puppeteer.launch({
        headless: this.parent.headless,
        userDataDir: this.parent.pathPrefix+'/data',
        args: [
          '--no-sandbox',
        ],
        slowMo: 10,
      });
      const context = this.#browser.defaultBrowserContext();
      await context.overridePermissions('https://www.facebook.com', ['notifications']);
      this.page = await this.#browser.newPage();
      await this.evadeHeadlessDetection();
      this.page.setViewport({width: 1200, height: 1600, deviceScaleFactor: 1});
    }

    console.log(`Getting posts for ${group.name} [for ${this.usr}]`);
    const url = `https://facebook.com/groups/${group.slug}`;
    let ret;
    console.log(url);
    await this.page.goto(url, {waitUntil: 'domcontentloaded'});
    console.log(`navigated to ${url}`);
    let skip = false;
    try {
      await this.page.waitForSelector(this.sel.posts.container, {
        timeout: 8000,
      });
    } catch (e) {
      console.log(this.usr, 'unable to load group. skipping!');
      skip = true;
    }
    try {
      if (!skip) {
        const posts = await this.page.evaluate((selectors, group) => {
          const posts = Array.from(
              document.querySelectorAll(selectors.container),
          );
          const gd = posts.filter((e) =>
            e.innerText.match(/looking for recommendations/i),
          );
          let cnt = 0;
          gd.map((e) => {
            e.classList.add('nalpa-rec');
            e.classList.add('nalpa--'+(++cnt));
          });
          cnt = 0;
          return gd.map((e) => {
            const child = e.querySelector(selectors.message);
            const permalink = e.querySelector(selectors.permalink).href;
            const id = permalink.match(/permalink\/(\d+)\//)[1];
            const {x, y, width, height} = e.getBoundingClientRect();
            return {
              query: child.innerText,
              id,
              permalink,
              idx: ++cnt,
              coords:
              {
                x, y, width, height,
              },
              group,
            };
          });
        }, this.sel.posts, group);
        if (posts.length > 0) {
          // console.log('requests for recommendations found');

          for (const post of posts) {
            await this.recPost(post);
          }
        } else {
          // console.log('no posts found');
        }
        ret = posts;
      }
    } catch (e) {
      console.log(this.usr, 'error getting requests: '+e );
    } finally {
      if (this.page) {
        await this.page.goto('about:blank');
        // await this.page.close();
        // this.page = undefined;
      }
      // await this.closeBrowser();
      browserLock.release();
      ret = null;
    }
    return ret;
  }
  /**
  * Group object
  * @typedef {Object} Group
  * @property {string} slug - url slug for group
  * @property {string} name - name of group
  */
  /**
   * Data detailing post.
   * @typedef {Object} Post
   * @property {string} query - detected recommendation query
   * @property (int) id - post ID
   * @property {string} permalink - permalink to post
   * @property {Group} group - group object
   * @property {Coordinates} coords - coordinates of element
   * @property {int} idx - index of recommendation
   */
  /**
   * check if post is in db
   * @param {Post} post - post object!
   */
  async checkPost(post) {
    const id = post.id;
    const group = post.group;
    const res = await this.col.findOne({post_id: id, group: group.slug, page: this.usr});
    return !!res;
  }

  /** *
   * iNsErT pOsT iNtO dB!1!
   * @param {Post} post - post object!
   */
  async addPost(post) {
    const {id, group, commentID, confidence} = post;
    console.log('adding post '+id+' for '+group.slug+' to db');
    const ret = await this.col.insertOne({
      group: group.slug,
      post_id: id,
      confidence,
      comment_id: commentID,
      page: this.usr,
    });
    if (ret.ops.length > 0) {
      console.log('post mcadded');
    } else {
      console.log('owo post unsuccessful add0r');
      return false;
    }
    return ret.insertedId;
  }
  /**
   * check if notify is in db
   * @param {Post} post - post object!
   */
  async checkNotify(post) {
    const page = this.usr;
    const id = post.id;
    const group = post.group;
    const res = await this.notify.findOne({
      post_id: id,
      group: group.slug,
      page,
    });
    return !!res;
  }

  /** *
   * iNsErT nOtIfY iNtO dB!1!
   * @param {Post} post - post object!
   */
  async addNotify(post) {
    const page = this.usr;
    const id = post.id;
    const group = post.group;
    console.log('adding notification for '+
      post.permalink+
      ' (for user: '+
      this.usr+
      ') to db',
    );
    const res = await this.notify.insertOne({
      page,
      group: group.slug,
      post_id: id,
    });
    if (res.ops.length > 0) {
      console.log('notify mcadded');
    } else {
      console.log('owo notify unsuccessful add0r');
    }
    return res.insertedId;
  }

  /**
  * click and wait...
  * @param {string} selector - css selector
  * @param {number} timeout - how long to wait uwu
  */
  async clickwait(selector, timeout) {
    if (!timeout) {
      timeout = 30000;
    }
    await this.page.waitForSelector(selector, {timeout: timeout});
    await this.page.click(selector);
  }
}

(async () => {
  const client = Redis.createClient();
  const args = process.argv.slice(2);
  const headless = !args.length;
  console.log('Headless:', headless);
  const lfr = await new Nalpa({
    headless,
  });

  client.on('message', (channel, msg) => {
    const json = JSON.parse(msg);
    const {origin} = json;
    if (process.env.DEBUG && !json.test) {
      return false;
    }
    if (json.dump) {
      if (origin) {
        lfr.dump(origin);
      } else {
        lfr.dumpWorkers();
      }
    } else if (json.retrain) {
      lfr.retrain(origin);
    } else if (json.reload) {
      if (origin) {
        lfr.reload(origin);
      }
    } else if (json.post) {
      const {id} = json;
      lfr.doPost(id);
    } else if (json.delete) {
      const {id} = json;
      lfr.deleteComment(id);
    } else {
      //console.log(`message received from ${channel}`);
      //console.dir(json);
      try {
        lfr.trigger(origin);
      } catch (e) {
        console.error(`Error reccin ${origin}: ${e}`);
      }
    }
  });
  client.subscribe('nalpa');
})();
