#!/usr/bin/env php
<?php
class Nalpar {
  private $channel = "nalpa";
  private $targetPage;
  private $redis;

  function __construct() {
    require_once "Predis/Autoloader.php";
    Predis\Autoloader::register();
    $this->redis = new Predis\Client(array(
      'host' => 'localhost',
      'scheme' => 'tcp',
      'port' => 6379
    )); 
  }

  function __destruct() {
    $this->redis->close();
  }

  public function pub($id, $comment="") {
    echo "posting! $id\n";
    $message = [
      'origin' => $id
    ];
    if(!empty($comment)) {
      $message["comment"] = $comment;
    }
    $this->redis->publish($this->channel, json_encode($message));
  }
}

$nlpr = new Nalpar();
$doc_root = $_SERVER["DOCUMENT_ROOT"];    
$hDir = realpath("$doc_root/..");    
$userName = basename($hDir);
$nlpr->pub($userName);

?>
