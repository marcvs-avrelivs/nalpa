<?php
class NalpusRex {
  private $channel = "nalpa";
  private $targetPage;
  private $redis;
  private $pubsub;
  private $subChannel;

  function __construct() {
    require_once "Predis/Autoloader.php";
    Predis\Autoloader::register();
    $this->redis = new Predis\Client(array(
      'host' => 'localhost',
      'scheme' => 'tcp',
      'port' => 6379
    )); 
  }

  function __destruct() {
    $this->redis->close();
  }

  public function pub($message) {
    $message = [
      "query" => $message,
    ];
    if(!empty($comment)) {
      $message["comment"] = $comment;
    }
    $this->redis->publish($this->channel, json_encode($message));
  }
  public function pubsub($channel) {
    try {
      $this->pubsub = $this->redis->pubSubLoop();   
      $this->pubsub->subscribe($channel);
      $this->subChannel = $channel;
    } catch (Exception $e) { 
      die("ERROR: " . $e->getMessage());
    }
  }
  public function subLoop() {
    $json = [];
    foreach ($this->pubsub as $i => $msg) {
      switch($msg->kind) {
        case 'subscribe':
          //subbd!
          break;
        case 'message':
          if($msg->channel == $this->subChannel) {
            $json = json_decode($msg->payload);
            $this->pubsub->stop();
          }
          break;
      }
    }
    return $json;
  }
}

$res = [];
$_POST["query"] = 'dongus';
if(!isset($_POST["query"])) {
  $res['error'] = 'no query provided';
} else {
  $nalpus = new NalpusRex();
  $query = $_POST['query']; 
  $nalpus->pubsub('nalpusrex');
  $nalpus->pub($query);
  $res = $nalpus->subLoop();
}
header("Content-type: text/json");
echo json_encode($res);
?>
