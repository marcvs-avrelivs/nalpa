class ueQue {
  #data = [];
  constructor(...args) {
    this.#data = args;
  }

  static fromArray(array) {
    const ret = new ueQue();
    ret.enq.apply(ret,array);
    return ret;
  }

  push(...args) {
    this.#data = this.#data.concat(args);
  }

  next() {
    const val = this.#data.shift();
    this.#data.push(val);
    return val;
  }

  enq(...args) {
    this.push.apply(this,args);
  }

  deq() {
    return this.next();
  }

  get() {
    return this.#data[0];
  }
  asArray() {
    return this.#data;
  }
  has(val) {
    return this.#data.includes(val);
  }
  length() { return this.#data.length; }
}

module.exports.ueQue = ueQue;
